from django.urls import path
from . import views


urlpatterns = [
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("", views.list_project, name="list_projects"),
]
