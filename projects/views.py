from django.shortcuts import render, get_object_or_404
from projects.models import Project
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from .forms import CreateProjectForm

# Create your views here.


def list_project(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(owner=request.user)
        print(projects)
        context = {
            "project_object": projects,
        }
        # return HttpResponse("Django is very difficult")

        return render(request, "projects/detail.html", context=context)
    else:
        return redirect("login")


@login_required(redirect_field_name="login")
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    print("checking the string", project.tasks.all())
    context = {
        "project_object": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required(redirect_field_name="login")
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
