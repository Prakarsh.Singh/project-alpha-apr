from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import CreateTaskForm

# Create your views here.


@login_required(redirect_field_name="login")
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required(redirect_field_name="login")
def show_my_tasks(request):
    tasks = request.user.tasks.all()
    context = {"my_tasks": tasks}
    return render(request, "tasks/my_tasks.html", context)
